
var estOuvert = false;

function EstMobile()
{
    return $(window).width() < 753;
}

//fait apparaitre le div
function AfficherDivMobile()
{
    if(!estOuvert)
    {
        $(".menu").show();
        estOuvert = true;
    }
    else if (EstMobile()) {
        $(".menu").hide();
        estOuvert = false;
    }
};

//gère lorsque le clic doit cacher le div
function CacherDivMobile(e) {
    if (EstMobile() &&
        $(e.target).closest(".menu").length === 0 &&
        $(e.target).closest("#btnConnHead").length === 0 &&
        $(e.target).closest(".btn.btn-mobile").length === 0){
        $(".menu").hide();
    }
};

$(document).ready(function() {
    $(document).on("click.window", function(e) {CacherDivMobile(e)});
    $(".btn.btn-mobile").on("click", AfficherDivMobile);
    if (EstMobile()) 
    {
        $(".menu").hide();
    }
    window.onresize = function()
    {
        if(EstMobile()) $(".menu").hide();
        if(!EstMobile()) $(".menu").show();
    }
});