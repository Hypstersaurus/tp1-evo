"use strict";

//fait apparaitre le div
function AfficherDiv()
{
    if ($("#divConn").is(":hidden")) {
        $("#divConn").show();
        $("#txtUtil").focus();
    }
    else $("#divConn").hide();

};

//gère lorsque le clic doit cacher le div
function CacherDiv(e) {
    if ($(e.target).closest("#divConn").length === 0 &&
        $(e.target).closest("#btnConnHead").length === 0 &&
        $(e.target).closest("#btnConnexion").length === 0){
        $("#divConn").hide();
    }
};

$(document).ready(function() {
    $(document).on("click.window", function(e) {CacherDiv(e)});
    $("#btnConnexion").click(AfficherDiv);
    $("#btnConnHead").click(AfficherDiv);
    $("#divConn").hide();
});