

var tiroir = $("#details-nouveautes");

function DetailClicHandler()
{
    if (tiroir.is(":hidden")) {
        tiroir.show();
    }
    else tiroir.hide();
}

$(document).ready(function() {
    tiroir.hide();
    $(".lien-detail").on("click", function() {DetailClicHandler()});
    $(".fa-times-circle").on("click", function() {tiroir.hide()});
});