
data = [
    "jijfrfr",
    "ajdjijf",
    "ororfk",
    "urnffrji",
    "eokgrgi",
    "ekjirjfr",
    "lwdkfjg",
    "frkigijjr",
    "dslkfdkjfdj",
    "dskfdnfdkjnfr",
    "kkrenmfrkfnrgrgrigrjgrgrgrngr",
    "smfd,fdmng,m",
    "rkjgrij",
    "k"
]

const maxSuggestions = 5;

const nomClasse = "listeSuggestion";

//true = a du contenu
var contenu = true;

//Affiche les suggestions s'il y en a à afficher
function AfficherSuggestions()
{
    if(contenu) 
    {
        $("#suggestions").show();
    }
}

//Gère si le clic se trouve dans le div des suggestions ou recherche
function CacherSuggestions(e)
{
    if ($(e.target).closest("#recherche").length === 0){
            $("#suggestions").hide();
    }
}

//gestion du contenu des suggestions à afficher selon la valeur dans la barre de recherche
function ChangerSuggestions()
{
    var i = 0;
    $("#suggestions").empty();
    var nbEnfants = 0;
    while(nbEnfants < maxSuggestions && i < data.length)
    {
        if (data[i].startsWith($("#recherche").val())) {
            $("#suggestions").append('<a class="'+nomClasse+'" id="sug'+i+'">'+data[i]+'</a>');
            EventHandlerSuggestions(i)
            nbEnfants++;
        }
        i++;
    }
    contenu = nbEnfants > 0;
    AfficherSuggestions();
}

//gère les différents events pour un élément de la liste des suggestions
function EventHandlerSuggestions(i)
{
    var val = $("#recherche").val();
    $("#sug"+i).on("mouseover", function() {
        $("#recherche").val(data[i]);
    });
    $("#sug"+i).on("click", function() {
        $("#recherche").val(data[i]);
        $("#sug"+i).off();
    });
    $("#sug"+i).on("mouseout", function() {
        $("#recherche").val(val);
    });
}

//Suggestions de base lorsque le champ est vide
function InitialiserSuggestions()
{
    $("#suggestions").empty();
    var i = 0;
    while(i < maxSuggestions && i < data.length)
    {
        $("#suggestions").append('<a class="'+nomClasse+'" id="sug'+i+'">'+data[i]+'</a>');
        EventHandlerSuggestions(i);
        i++;
    }
    contenu = true;
}

$(document).ready(function() {
    InitialiserSuggestions();
    $("#suggestions").hide();
    $("#recherche").on("focus", function() {AfficherSuggestions()});
    $("#recherche").on("input", function() {ChangerSuggestions()});
    $(document).on("click.window", function(e) {CacherSuggestions(e)});
});